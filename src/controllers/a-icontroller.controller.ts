import {Count, CountSchema, Filter, FilterExcludingWhere, repository, Where} from '@loopback/repository';
import {del, get, getModelSchemaRef, param, patch, post, put, requestBody} from '@loopback/rest';
import {Formapischeme} from '../models';
import {FormapischemeRepository} from '../repositories';
export class AIcontrollerController {
  constructor(
    @repository(FormapischemeRepository)
    public formapischemeRepository: FormapischemeRepository,
  ) {}




  @get('/formapischemes/getallnames/{name}', {
    responses: {
      '200': {
        description: 'Formapischeme model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async getAInames(
    @param.path.string('name') name: string,
    @param.filter(Formapischeme) filter?: Filter<Formapischeme>,
  ): Promise<Formapischeme[]> {
    console.log(name)
    //Script to delete all the data but the id and AI name.
    filter = {
      where: {users: {inq: [name]}},
      fields: {
        id: true, AIname: true, desc: true, status: true
      }
    }
    let alldata = this.formapischemeRepository.find(filter);

    return alldata

  }



  //***********Function to Individually Add ArtBoard to Project*********
  @post('/AddArtBoard/{id}', {
    responses: {
      '200': {
        description: 'Formapischeme model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Formapischeme),
          },
        },
      },
    },
  })
  async addArtBoard(
    @requestBody({
      description: 'request object value',
      required: true,
      content: {
        'application/json': {
          schema: {type: 'object'},
        },
        'application/x-www-form-urlencoded': {
          schema: {type: 'object'},
        },
        'application/xml': {
          schema: {type: 'object'},
        },
      },
    })
    data: object,
  ): Promise<Object> {
    //Extract AI name from request body
    // Object Form {ProjectId:'',ArtBoardObject}
    let ProjectId = Object(data)['ProjectId'].toString(),
      outcome
    //custom filters
    let checkExistanceFilter = {
      where: {
        and: [
          {id: ProjectId}
          ,
          {"ArtBoards.id": Object(data)['ArtBoardObject'].id}
        ]
      }

    }

    let response = new Promise<Object>(async (resolve, rejects) => {
      outcome = await this.formapischemeRepository.find(checkExistanceFilter);
      resolve(outcome)
    }).then(async (res: any) => {

      try {
        let Action = {
          $addToSet: {
            ArtBoards: Object(data)['ArtBoardObject']
          }
        }
        let Filter = {id: ProjectId}

        let Action2 = {
          $set: {
            "ArtBoards.$": Object(data)['ArtBoardObject']
          }
        }

        if (res[0] == undefined) {
          return await this.formapischemeRepository.updateAll(Action, Filter)
        } else {

          return await this.formapischemeRepository.updateAll(Action2, checkExistanceFilter.where)

        }
      } catch (e) {
        return e
      }


    })


    return response
  }


  @post('/getAIByNameWeb', {
    responses: {
      '200': {
        description: 'Formapischeme model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Formapischeme),
          },
        },
      },
    },
  })
  async getAImini(
    @requestBody({
      description: 'request object value',
      required: true,
      content: {
        'application/json': {
          schema: {type: 'object'},
        },
        'application/x-www-form-urlencoded': {
          schema: {type: 'object'},
        },
        'application/xml': {
          schema: {type: 'object'},
        },
      },
    })
    data: object,
  ): Promise<Object> {
    //Extract AI name from request body
    let AIname, id, filter
    //custom filter
    let f = {
      id: true, AIname: true, schemelist: true
    }




    if (Object(data)['AIname']) {
      AIname = Object(data)['AIname']
      filter = {
        fields: f,
        where: {
          "AIname": AIname
        }

      }
    }

    if (Object(data)['id']) {
      id = Object(data)['id']
      filter = {
        fields: f,
        where: {
          "id": id
        }

      }

    }


    return this.formapischemeRepository.find(filter);



  }




  @post('/formapischemes', {
    responses: {
      '200': {
        description: 'Formapischeme model instance',
        content: {'application/json': {schema: getModelSchemaRef(Formapischeme)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Formapischeme, {
            title: 'NewFormapischeme',
            exclude: ['id'],
          }),
        },
      },
    })
    formapischeme: Omit<Formapischeme, 'id'>,
  ): Promise<Formapischeme> {
    return this.formapischemeRepository.create(formapischeme);
  }


  @get('/formapischemes', {
    responses: {
      '200': {
        description: 'Array of Formapischeme model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Formapischeme, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Formapischeme) filter?: Filter<Formapischeme>,
  ): Promise<Formapischeme[]> {
    return this.formapischemeRepository.find(filter);
  }

  @patch('/formapischemes', {
    responses: {
      '200': {
        description: 'Formapischeme PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Formapischeme, {partial: true}),
        },
      },
    })
    formapischeme: Formapischeme,
    @param.where(Formapischeme) where?: Where<Formapischeme>,
  ): Promise<Count> {
    return this.formapischemeRepository.updateAll(formapischeme, where);
  }

  @get('/formapischemes/{id}', {
    responses: {
      '200': {
        description: 'Formapischeme model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Formapischeme, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Formapischeme, {exclude: 'where'}) filter?: FilterExcludingWhere<Formapischeme>
  ): Promise<Formapischeme> {
    return this.formapischemeRepository.findById(id, filter);
  }

  @patch('/formapischemes/{id}', {
    responses: {
      '204': {
        description: 'Formapischeme PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Formapischeme, {partial: true}),
        },
      },
    })
    formapischeme: Formapischeme,
  ): Promise<void> {

    try {
      return await this.formapischemeRepository.updateById(id, formapischeme);
    } catch (e) {
      console.log('EEROR', formapischeme, e)
      return e
    }

  }

  @put('/formapischemes/{id}', {
    responses: {
      '204': {
        description: 'Formapischeme PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() formapischeme: Formapischeme,
  ): Promise<void> {
    await this.formapischemeRepository.replaceById(id, formapischeme);
  }

  @del('/formapischemes/{id}', {
    responses: {
      '204': {
        description: 'Formapischeme DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.formapischemeRepository.deleteById(id);
  }
}

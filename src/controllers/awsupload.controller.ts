import {inject} from '@loopback/core'
import {post, Request, requestBody, Response, RestBindings} from '@loopback/rest'
import AWS from 'aws-sdk'
import multer from 'multer'
import stream from 'stream'

const {Duplex} = stream

function bufferToStream(buffer: any) {
  const duplexStream = new Duplex()
  duplexStream.push(buffer)
  duplexStream.push(null)
  return duplexStream
}

// const config = {
//   region: process.env.S3_REGION,
//   accessKeyId: process.env.S3_ACCESSKEYID,
//   secretAccessKey: process.env.S3_SECRETACCESSKEY,
//   endpoint: process.env.S3_ENDPOINT
// }
// const s3 = new AWS.S3(config)

const s3 = new AWS.S3({
  accessKeyId: 'AKIAJHIRWAWS3WD2QERA',
  secretAccessKey: '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'
});

const bucketName = 'aibuilderuserdocs'
const projectBucketName = `dcubedprojectsbucket`



export class AwsuploadController {
  constructor() {}

  // /buckets/{bucketName}/upload
  @post('/buckets/upload', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: '',
      },
    },
  })
  async upload(
    // @param.path.string('bucketName') bucketName: string
    @requestBody({

      description: 'multipart/form-data value.',
      required: true,
      content: {
        'multipart/form-data': {
          // Skip body parsing
          'x-parser': 'stream',
          schema: {type: 'object'},
        },
      },
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {

    return new Promise<object>((resolve, reject) => {

      const storage = multer.memoryStorage()
      const upload = multer({storage})

      let buff = new Buffer(Object(request).headers.data.toString());
      let base64data = buff.toString('base64');

      upload.any()(request, response, async err => {
        if (err) reject(err)
        else {
          let res = new Array()
          for (const file of (request as any).files) {
            const params = {
              Bucket: bucketName,
              Key: base64data + file.originalname, // File name you want to save as in S3
              Body: bufferToStream(file.buffer),
              ACL: "public-read"
            }
            try {
              const stored = await s3.upload(params).promise()
              res.push(stored)
            } catch (err) {
              reject(err)
            }
          }
          resolve(res)
        }
      })
    })
  }

  //Create Project Folder
  @post('/buckets/createproject', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: '',
      },
    },
  })
  async createfolder(
    // @param.path.string('bucketName') bucketName: string
    @requestBody({
      description: 'multipart/form-data value.',
      required: true,
      content: {
        'application/json': {
          schema: {type: 'object'},
        },
      },
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {

    return new Promise<object>(async (resolve, reject) => {
      let obj = Object(request)
      let prefix = obj.prefix
      let projectname = obj.projectname

      let res = new Array()
      const params = {
        Bucket: projectBucketName,
        Key: `${prefix}${projectname}/`, // File name you want to save as in S3
        Body: '',
        ACL: "public-read"
      }

      try {
        const stored = await s3.upload(params).promise()
        res.push(stored)
      } catch (err) {
        reject(err)
      }

      resolve(res)


    })
  }

  //Add File to Project Folder
  @post('/buckets/addFileToFolder', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: '',
      },
    },
  })
  async addHTMLFileToProject(
    // @param.path.string('bucketName') bucketName: string
    @requestBody({

      description: 'multipart/form-data value.',
      required: true,
      content: {
        'multipart/form-data': {
          // Skip body parsing
          'x-parser': 'stream',
          schema: {type: 'object'},
        },
      },
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {

    return new Promise<object>((resolve, reject) => {

      const storage = multer.memoryStorage()
      const upload = multer({storage})
      let dir = Object(request).headers.data.toString()

      upload.any()(request, response, async err => {
        if (err) reject(err)
        else {
          let res = new Array()
          for (const file of (request as any).files) {
            const params = {
              Bucket: projectBucketName,
              Key: `${dir}/${file.originalname}`,
              ContentType: 'text/html',// File name you want to save as in S3
              Body: bufferToStream(file.buffer),
              ACL: "public-read"
            }
            try {
              const stored = await s3.upload(params).promise()
              res.push(stored)
            } catch (err) {
              reject(err)
            }
          }
          resolve(res)
        }
      })
    })
  }

  @post('/buckets/addfile_type_any', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: '',
      },
    },
  })
  async addFileToProject(
    // @param.path.string('bucketName') bucketName: string
    @requestBody({

      description: 'multipart/form-data value.',
      required: true,
      content: {
        'multipart/form-data': {
          // Skip body parsing
          'x-parser': 'stream',
          schema: {type: 'object'},
        },
      },
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {

    return new Promise<object>((resolve, reject) => {

      const storage = multer.memoryStorage()
      const upload = multer({storage})
      let dir = Object(request).headers.data.toString()

      upload.any()(request, response, async err => {
        if (err) reject(err)
        else {
          let res = new Array()
          for (const file of (request as any).files) {
            const params = {
              Bucket: projectBucketName,
              Key: `${dir}/${file.originalname}`,
              ContentType: `${file.type}`,// File name you want to save as in S3
              Body: bufferToStream(file.buffer),
              ACL: "public-read"
            }
            try {
              const stored = await s3.upload(params).promise()
              res.push(stored)
            } catch (err) {
              reject(err)
            }
          }
          resolve(res)
        }
      })
    })
  }


  //Delete Object from S3
  @post('/buckets/delete', {
    responses: {
      200: {
        content: {
          'application/json': {
            schema: {
              type: 'object',
            },
          },
        },
        description: '',
      },
    },
  })
  async delete(
    // @param.path.string('delete') deleteName: string,
    @requestBody({
      description: 'request object value',
      required: true,
      content: {
        'application/json': {
          schema: {type: 'object'},
        },
        'application/x-www-form-urlencoded': {
          schema: {type: 'object'},
        },
        'application/xml': {
          schema: {type: 'object'},
        },
      },
    })
    request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response,
  ): Promise<object> {


    return new Promise<object>(async (resl, rej) => {
      let req = Object(request).key
      // console.log(req)
      const params = {
        Bucket: bucketName,
        Key: req, // File name you want to delte  in S3

      }
      try {
        const stored = await s3.deleteObject(params).promise()
        resl(stored)
      } catch (err) {
        rej(err)
      }
    })


  }








}





import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,

  getModelSchemaRef, param,




  patch, post,






  put,

  requestBody
} from '@loopback/rest';
import {Adobexd} from '../models';
import {AdobexdRepository} from '../repositories';

export class AdobeControllerController {
  constructor(
    @repository(AdobexdRepository)
    public adobexdRepository: AdobexdRepository,
  ) {}

  @post('/adobexds', {
    responses: {
      '200': {
        description: 'Adobexd model instance',
        content: {'application/json': {schema: getModelSchemaRef(Adobexd)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Adobexd, {
            title: 'NewAdobexd',

          }),
        },
        'application/x-www-form-urlencoded': {
          schema: {type: 'object'},
        }
      },
    })
    adobexd: Adobexd,
  ): Promise<Adobexd> {
    return this.adobexdRepository.create(adobexd);
  }

  @get('/adobexds/count', {
    responses: {
      '200': {
        description: 'Adobexd model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Adobexd) where?: Where<Adobexd>,
  ): Promise<Count> {
    return this.adobexdRepository.count(where);
  }

  @get('/adobexds/{username}', {
    responses: {
      '200': {
        description: 'Array of Adobexd model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Adobexd, {includeRelations: true}),
            },
          },
          'application/x-www-form-urlencoded': {
            schema: {type: 'object'},
          }
        },
      },
    },
  })
  async find(
    @param.path.string('username') username: string,
    @param.filter(Adobexd) filter?: Filter<Adobexd>,
  ): Promise<Adobexd[]> {
    filter = {
      where: {username: username}
    }
    return this.adobexdRepository.find(filter);
  }

  @patch('/adobexds', {
    responses: {
      '200': {
        description: 'Adobexd PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Adobexd, {partial: true}),
        },
        'application/x-www-form-urlencoded': {
          schema: {type: 'object'},
        }
      },
    })
    adobexd: Adobexd,
    @param.where(Adobexd) where?: Where<Adobexd>,
  ): Promise<Count> {
    return this.adobexdRepository.updateAll(adobexd, where);
  }

  @get('/adobexds/{id}', {
    responses: {
      '200': {
        description: 'Adobexd model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Adobexd, {includeRelations: true}),
          },
          'application/x-www-form-urlencoded': {
            schema: {type: 'object'},
          }
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(Adobexd, {exclude: 'where'}) filter?: FilterExcludingWhere<Adobexd>
  ): Promise<Adobexd> {
    return this.adobexdRepository.findById(id, filter);
  }

  @patch('/adobexds/{id}', {
    responses: {
      '204': {
        description: 'Adobexd PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Adobexd, {partial: true}),
        },
        'application/x-www-form-urlencoded': {
          schema: {type: 'object'},
        }
      },
    })
    adobexd: Adobexd,
  ): Promise<void> {
    await this.adobexdRepository.updateById(id, adobexd);
  }

  @put('/adobexds/{id}', {
    responses: {
      '204': {
        description: 'Adobexd PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() adobexd: Adobexd,
  ): Promise<void> {
    await this.adobexdRepository.replaceById(id, adobexd);
  }

  @del('/adobexds/{id}', {
    responses: {
      '204': {
        description: 'Adobexd DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.adobexdRepository.deleteById(id);
  }
}

import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Formapischeme extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  AIname: string;

  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'any',
  })
  functions?: object;

  @property({
    type: 'string',
  })
  html?: string;

  @property({
    type: 'any',
  })
  schemelist?: any;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Formapischeme>) {
    super(data);
  }
}

export interface FormapischemeRelations {
  // describe navigational properties here
}

export type FormapischemeWithRelations = Formapischeme & FormapischemeRelations;

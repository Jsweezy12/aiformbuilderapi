import {Entity, model, property} from '@loopback/repository';

@model({settings: {strict: false}})
export class Adobexd extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  id?: string;

  @property({
    type: 'array',
    itemType: 'object',
    required: true,
  })
  html: object[];

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<Adobexd>) {
    super(data);
  }
}

export interface AdobexdRelations {
  // describe navigational properties here
}

export type AdobexdWithRelations = Adobexd & AdobexdRelations;

import {DefaultCrudRepository} from '@loopback/repository';
import {Formapischeme, FormapischemeRelations} from '../models';
import {AiformDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class FormapischemeRepository extends DefaultCrudRepository<
  Formapischeme,
  typeof Formapischeme.prototype.id,
  FormapischemeRelations
> {
  constructor(
    @inject('datasources.AIFORM') dataSource: AiformDataSource,
  ) {
    super(Formapischeme, dataSource);
  }
}

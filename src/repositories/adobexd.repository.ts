import {DefaultCrudRepository} from '@loopback/repository';
import {Adobexd, AdobexdRelations} from '../models';
import {AiformDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class AdobexdRepository extends DefaultCrudRepository<
  Adobexd,
  typeof Adobexd.prototype.id,
  AdobexdRelations
> {
  constructor(
    @inject('datasources.AIFORM') dataSource: AiformDataSource,
  ) {
    super(Adobexd, dataSource);
  }
}

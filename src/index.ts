import {ApplicationConfig} from '@loopback/core';
import {FormbuilderapiApplication} from './application';
var fs = require('fs')

export {FormbuilderapiApplication};

export async function main(options: ApplicationConfig = {}) {
  options.rest = {

    // ** Delcare port with :port: 3100,//
    requestBodyParser: {json: {limit: '20GB'}, text: {limit: '20GB'}},
    port: 80
    // protocol: 'https',
    // key: fs.readFileSync('./key.pem'),
    // cert: fs.readFileSync('./cert.pem'),
  };

  // const optionssl = {
  //   rest: {
  //     protocol: 'https',
  //     key: fs.readFileSync('./cert/my-site.com.key'),
  //     cert: fs.readFileSync('./cert/my-site.com.crt'),
  //   }
  // };

  const app = new FormbuilderapiApplication(options);
  await app.boot();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);
  console.log(`Try ${url}/ping`);

  return app;
}
